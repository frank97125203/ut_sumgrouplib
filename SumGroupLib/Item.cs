﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SumGroupLib
{
    public struct Item
    {
        public int Cost { get; init; }
        public int Price { get; init; }
        public string Name { get; set; }
    }
}
