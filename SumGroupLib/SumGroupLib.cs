﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SumGroupLib
{
    public static class SumGroupLib
    {
        public static IEnumerable<int> SumGroup<TSource>(this IEnumerable<TSource> source, int groupSize, Func<TSource, int> selector)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source Error");
            }

            if (selector == null)
            {
                throw new ArgumentNullException("selector Error");
            }
            if (groupSize <= 0)
            {
                throw new ArgumentException("groupSize <= 0");
            }

            int index = 0;
            for (int i = source.Count(); i > 0; i -= groupSize, index += groupSize)
            {
                
                yield return source.Skip(index).Take(groupSize).ToList().Sum(selector);
            }
        }
    }
}
