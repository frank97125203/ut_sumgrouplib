using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SumGroupLib
{
    public class Tests
    {
        List<Item> items;
        [SetUp]
        public void Setup() => items = new List<Item>
            {
                new() {Cost = 10, Price = 130, Name = "A"},
                new() {Cost = 21, Price = 150, Name = "B"},
                new() { Cost = 32, Price = 200, Name = "C" },
                new() { Cost = 43, Price = 220, Name = "D" },
                new() { Cost = 54, Price = 250, Name = "E" },
                new() { Cost = 65, Price = 260, Name = "F" },
                new() { Cost = 76, Price = 300, Name = "G" },
                new() { Cost = 87, Price = 410, Name = "H" },
                new() { Cost = 98, Price = 660, Name = "I" },
                new() { Cost = 109, Price = 730, Name = "J" }
            };

        [Test]
        public void SumGroupTestByPriceSize4()
        {
            var expected = new List<int>() { 700, 1220, 1390 };
            var result = items.SumGroup(4, product => product.Price).ToList();
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void SumGroupTestByCostSize3()
        {
            var expected = new List<int>() { 63, 162, 261, 109 };
            var result = items.SumGroup(3, product => product.Cost).ToList();
            Assert.AreEqual(expected, result);
        }
        [Test]
        public void SumGroupTestBySize0()
        {
            Action act = () => items.SumGroup(0, product => product.Cost).ToList();
            act.Should().Throw<ArgumentException>().WithMessage("groupSize <= 0");
        }

    }
}